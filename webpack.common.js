const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './dev/index.js',
  output: {
    path: path.resolve(__dirname, 'dev'),
    filename: 'bundle.js',
    publicPath:  process.env.NODE_ENV === 'development' ? '/' : './'
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        loader: 'html-loader',
        options: {
          root: path.resolve(__dirname, 'dev')
        }
      },
      { 
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', options: { minimize: true } },
            { loader: 'sass-loader' }
          ]
        })
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      },
      {
        test: /\.(png|jp(e*)g|svg)$/,  
        use: [{
          loader: 'file-loader',
          options: { 
            name: '[name].[ext]',
            outputPath: 'img/',
          } 
        }]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        loader: "file-loader",
        options: {
          // publicPath: '/static/build',
          outputPath: 'fonts/',
          name: '[name].[ext]',
        }
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './dev/html/index.html'
    }),
    new HtmlWebpackPlugin({
      filename: 'plug.html',
      template: './dev/html/plug.html'
    }),
    new ExtractTextPlugin('style.css')
  ]
};