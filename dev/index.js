import style from './scss/index.scss';
import Rellax from 'rellax';
import $ from 'jquery';


// mobile menu
$(function() {
	$('.js-burger-link').on('click', function(){
		$(this).toggleClass('_active');
		$('.js-header').toggleClass('menu-open');
	})
});

// parallax
$(function(){
	new Rellax('.rellax');
});

// scrollto element
$(function(){
	$('.js-scrollto').on('click', function(e) {
		e.preventDefault();

		const scrollElData = $(this).data('scrollto');
		const headerHeight = $('.js-header').height();

		if (typeof scrollElData === 'string') {
			const scrollEl = $(`#${scrollElData}`);
			$('html, body').stop().animate({ scrollTop: scrollEl.offset().top - (headerHeight + 30) }, '500');
		}
	})
});

// mapgotocoords
$(function(){
	$(".js-mapto").on('click', function(){	
		if (window.myMap) {
			const parseCoords = JSON.parse(`[${$(this).data('coords')}]`);
			window.myMap.panTo(parseCoords, { delay: 500 });
		}
	});
});